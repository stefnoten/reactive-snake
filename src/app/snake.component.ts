import {AfterViewInit, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import {BoardComponent} from './board.component';
import {Cell} from './model/cell';
import {Snake} from './model/snake';

@Component({
  selector: 'snk-snake',
  template: `
    <canvas #canvas [width]="board.width" [height]="board.height"></canvas>
  `,
  styles: [`
  `]
})
export class SnakeComponent implements AfterViewInit, OnChanges {

  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;

  @Input() color = '#4e72aa';
  @Input() snake: Snake | null;

  constructor(public board: BoardComponent) {
  }

  ngAfterViewInit(): void {
    this.repaint();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.canvas) {
      this.repaint();
    }
  }

  private repaint() {
    this.clear();
    if (this.snake) {
      this.paintHead(this.snake.head);
      this.snake.tail.forEach(cell => this.paintTail(cell));
    }
  }

  private clear() {
    this.context.clearRect(0, 0, this.board.width, this.board.height);
  }

  private get context() {
    return this.canvas.nativeElement.getContext('2d');
  }

  private paintHead(cell: Cell) {
    this.paintTail(cell);
    this.paint(cell, 'rgba(0, 0, 0, 0.7)');
  }

  private paintTail(cell: Cell) {
    this.paint(cell, this.color);
  }

  private paint(cell: Cell, color: string) {
    this.context.fillStyle = color;
    const {innerCellSize, cellSize, cellPadding} = this.board;
    this.context.fillRect(
      cell.x * cellSize + cellPadding,
      cell.y * cellSize + cellPadding,
      innerCellSize,
      innerCellSize
    );
  }
}
