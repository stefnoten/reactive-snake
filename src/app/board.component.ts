import {Component, Input} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'snk-board',
  template: `
    <div class="container" [style.width]="width + 'px'">
      <div class="header">
        <ng-content select="snk-header-bar"></ng-content>
      </div>
      <div class="board" [style.height]="height + 'px'">
        <ng-content></ng-content>
        <div class="game-over-container" *ngIf="gameOver" [@gameOver]>
          <div class="game-over">Game over!</div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .container {
      border: 2px solid gray;
      background: lightgray;
      display: block;
      margin: 0 auto;
    }

    .header {
      border-bottom: 2px solid gray;
      padding: 0.3em;
      font-weight: bold;
      color: dimgray;
    }

    .board {
      position: relative;
    }

    .game-over-container {
      background: rgba(0, 0, 0, 0.7);
      display: flex;
      justify-content: center;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
    }

    .game-over {
      color: #4b0706;
      font-size: 5em;
      font-weight: bold;
      align-self: center;
    }
  `],
  animations: [
    trigger('gameOver', [
      state('void', style({ opacity: 0 })),
      state('*', style({ opacity: 1 })),
      transition('void <=> *', animate('100ms ease-out'))
    ])
  ]
})
export class BoardComponent {

  @Input() columns: number;
  @Input() rows: number;
  @Input() cellSize = 20;
  @Input() cellPadding = 1;
  @Input() gameOver = false;

  get innerCellSize() {
    return this.cellSize - 2 * this.cellPadding;
  }

  get width() {
    return this.columns * this.cellSize;
  }

  get height() {
    return this.rows * this.cellSize;
  }
}
