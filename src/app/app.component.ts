import {Component} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Snake} from './model/snake';
import {Apple} from './model/apple';

@Component({
  selector: 'snk-root',
  template: `
    <snk-board [columns]="boardSize.x" [rows]="boardSize.y" [gameOver]="gameOver$ | async">
      <snk-header-bar>
        <snk-score [score]="score$ | async"></snk-score>
      </snk-header-bar>
      <snk-snake [snake]="snake$ | async"></snk-snake>
      <snk-apple *ngFor="let apple of (apples$ | async)" [apple]="apple"></snk-apple>
    </snk-board>
  `
})
export class AppComponent {

  readonly boardSize = {x: 30, y: 30};

  gameOver$ = of(false);

  /**
   * Exercise 1: Use a time interval to generate a random snake per tick (e.g. 150ms).
   * Exercise 2: Make the snake move in a single direction.
   * Exercise 3: Turn the snake based on arrow keys.
   * Exercise 4: Make sure the snake can't reverse its direction.
   * (Exercise 5: see below)
   * Exercise 6: Make the snake grow when it eats an apple.
   * (Exercise 7: see below)
   * Exercise 8: Finish the game when the snake starts eating itself.
   */
  snake$: Observable<Snake> = of(Snake.randomIn(this.boardSize));

  /**
   * Exercise 5: Replace the apple with a new one when it is eaten.
   */
  apples$: Observable<Apple[]> = of([Apple.randomIn(this.boardSize)]);

  /**
   * Exercise 7: Increase the score when an apple has been eaten.
   */
  score$ = of(0);
}
