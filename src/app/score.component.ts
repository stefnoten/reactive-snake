import {Component, Input} from '@angular/core';

@Component({
  selector: 'snk-score',
  template: `
    <span>Score: {{score}}</span>
  `
})
export class ScoreComponent {
  @Input() score = 0;
}
