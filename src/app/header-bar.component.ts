import {Component} from '@angular/core';

@Component({
  selector: 'snk-header-bar',
  template: `
    <ng-content></ng-content>
  `
})
export class HeaderBarComponent {
}
