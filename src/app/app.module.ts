import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BoardComponent} from './board.component';
import {SnakeComponent} from './snake.component';
import {AppleComponent} from './apple.component';
import {HeaderBarComponent} from './header-bar.component';
import {ScoreComponent} from './score.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    SnakeComponent,
    AppleComponent,
    HeaderBarComponent,
    ScoreComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
