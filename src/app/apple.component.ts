import {Component, Input} from '@angular/core';
import {BoardComponent} from './board.component';
import {Apple} from './model/apple';

@Component({
  selector: 'snk-apple',
  template: `
    <div
      class="apple"
      [style.width]="board.innerCellSize + 'px'"
      [style.height]="board.innerCellSize + 'px'"
      [style.left]="apple.x * board.cellSize + board.cellPadding + 'px'"
      [style.top]="apple.y * board.cellSize + board.cellPadding + 'px'"
    >
    </div>
  `,
  styles: [`
    .apple {
      display: block;
      position: absolute;
      background: no-repeat url('assets/apple.png');
      background-size: contain;
    }
  `]
})
export class AppleComponent {
  @Input() apple: Apple;

  constructor(public board: BoardComponent) {
  }
}
