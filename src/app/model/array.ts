export function withoutLast<T>(array: T[]) {
  const copy = [...array];
  copy.pop();
  return copy;
}

export function partition<T>(array: T[], predicate: (T) => boolean): [T[], T[]] {
  const trueValues = [] as T[];
  const falseValues = [] as T[];
  for (const value of array) {
    if (predicate(value)) {
      trueValues.push(value);
    } else {
      falseValues.push(value);
    }
  }
  return [trueValues, falseValues];
}
