import {Vector} from './vector';
import {Cell} from './cell';

export class Apple {
  static randomIn(size: Vector) {
    return new Apple(Cell.randomIn(size));
  }

  constructor(public readonly cell: Cell) {
  }

  get x() {
    return this.cell.x;
  }

  get y() {
    return this.cell.y;
  }
}
