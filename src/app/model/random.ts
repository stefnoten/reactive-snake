export function randomInt(minInclusive: number, maxExclusive: number) {
  return minInclusive + Math.floor((maxExclusive - minInclusive) * Math.random());
}
