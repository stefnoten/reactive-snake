import {Cell} from './cell';
import {Direction} from './direction';
import {Vector} from './vector';
import {withoutLast} from './array';
import {Apple} from './apple';

export class Snake {
  static randomIn(size: Vector) {
    return new Snake([Cell.randomIn(size)]);
  }

  private constructor(public readonly cells: Cell[]) {
    if (cells.length === 0) {
      throw new Error('A snake should consist of at least one cell');
    }
  }

  move(direction: Direction, boardSize: Vector): Snake {
    return new Snake([this.newHead(direction, boardSize), ...this.shorten()]);
  }

  moveAndGrow(direction: Direction, boardSize: Vector): Snake {
    return new Snake([this.newHead(direction, boardSize), ...this.cells]);
  }

  canEat(apple: Apple) {
    return this.cells.some(cell => cell.equals(apple.cell));
  }

  get isEatingItself() {
    return this.tail.some(cell => this.head.equals(cell));
  }

  get head() {
    return this.cells[0];
  }

  get tail() {
    const [, ...tail] = this.cells;
    return tail;
  }

  private newHead(direction: Direction, boardSize: Vector) {
    return this.head.neighbour(direction, boardSize);
  }

  private shorten() {
    return withoutLast(this.cells);
  }
}
