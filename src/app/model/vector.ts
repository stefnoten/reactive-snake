export interface Vector {
  readonly x: number;
  readonly y: number;
}
