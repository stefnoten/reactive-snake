import {Direction} from './direction';
import {Vector} from './vector';
import {randomInt} from './random';

export class Cell implements Vector {
  static randomIn(size: Vector) {
    return new Cell(randomInt(0, size.x), randomInt(0, size.y));
  }

  constructor(public readonly x: number,
              public readonly y: number) {
  }

  neighbour(direction: Direction, wrappingSize: Vector) {
    return this.neighbourWithoutWrapping(direction).wrap(wrappingSize);
  }

  equals(other: Cell) {
    return this.x === other.x && this.y === other.y;
  }

  private wrap(size: Vector) {
    return new Cell(modulo(this.x, size.x), modulo(this.y, size.y));
  }

  private neighbourWithoutWrapping(direction: Direction) {
    return this.plus(direction);
  }

  private plus(other: Vector) {
    return new Cell(this.x + other.x, this.y + other.y);
  }
}

function modulo(value: number, n: number) {
  return ((value % n) + n) % n;
}
