import {Vector} from './vector';

export class Direction implements Vector {
  static LEFT = new Direction(-1, 0);
  static UP = new Direction(0, -1);
  static RIGHT = new Direction(1, 0);
  static DOWN = new Direction(0, 1);

  private constructor(public readonly x: number, public readonly y: number) {
  }

  isOppositeOf(other: Direction) {
    return this.x === -other.x && this.y === -other.y;
  }
}

export function directionFromEvent(e: KeyboardEvent): Direction | null {
  return directionFromKey(e.key);
}

const keyDirections = {
  ArrowLeft: Direction.LEFT,
  ArrowUp: Direction.UP,
  ArrowRight: Direction.RIGHT,
  ArrowDown: Direction.DOWN,
};

function directionFromKey(key: string): Direction | null {
  return keyDirections[key] || null;
}
